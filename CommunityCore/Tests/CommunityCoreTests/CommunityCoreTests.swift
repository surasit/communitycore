    import XCTest
    @testable import CommunityCore

    final class CommunityCoreTests: XCTestCase {
        func testExample() {
            // This is an example of a functional test case.
            // Use XCTAssert and related functions to verify your tests produce the correct
            // results.
            XCTAssertEqual(CommunityCore.shared.getCommunityVersion(), "Hello, World!")
        }
    }
