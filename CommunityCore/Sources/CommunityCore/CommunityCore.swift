//
//  CommunityCore.swift
//
//
//  Created by Surasit Intawong on 10/9/2564 BE.
//

import Foundation

public class CommunityCore {
    static let shared = CommunityCore()
    private init() { }
    
    public func getCommunityVersion() -> String? {
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        return appVersion
    }
}
