//
//  ContentView.swift
//  example-trueid-community
//
//  Created by Surasit Intawong on 10/9/2564 BE.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Button(action: {
            //SPM
            
        }, label: {
            Text("Open Community")
        })
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
